Minimal reproducible example of a bug described in [this](https://discuss.pytorch.org/t/quantization-aware-training-with-dataparallel-crashes-randomly/103545) discussion

Requirements: pyTorch 1.7.0, tqdm

Usage:

`python run.py original`  - runs original EfficientNet model, bug is not reproduced

or

`python run.py qfriendly` - runs my implementation of quantization-friendly EfficientNet, which fails with assertionError randomly

My implementation differs in the following:
1) Multiplication and addition operations were replaced with
    quantization-friendly FloatFunctional.
3) Other MBConvBlocks are used: they contain Conv2D instance instead of conv2d() function call
4) Paddings changed to be symmetrical about kernel size (3x3 kernel => 1x1 padding; 5x5 kernel => 2x2 padding).
No more ZeroPad2D