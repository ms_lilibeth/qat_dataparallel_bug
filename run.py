from argparse import ArgumentParser

import torch
import tqdm
from torch import nn

from efficientnet.model import create as create_original
from qefficientnet.model import create as create_qfriendly

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('effnet_type', choices=['original', 'qfriendly'])
    args = parser.parse_args()

    device = torch.device('cuda')
    if args.effnet_type == 'original':
        model = create_original('efficientnet-b2')
    else:
        model = create_qfriendly('efficientnet-b2')

    model = nn.DataParallel(model)
    model.train()
    model.to(device)

    qconfig = torch.quantization.get_default_qat_qconfig('fbgemm')
    model.qconfig = qconfig
    torch.quantization.prepare_qat(model, inplace=True)

    for _ in tqdm.tqdm(range(10)):
        _input = torch.randn(16, 3, 256, 256).to(device)
        output = model(_input)
