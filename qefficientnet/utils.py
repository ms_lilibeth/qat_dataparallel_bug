from functools import partial

import torch
from torch import nn
from torch.nn.quantized import FloatFunctional
from torch.utils import model_zoo

from efficientnet.utils import url_map


def get_same_padding_conv2d(image_size=None):
    if image_size is None:
        raise RuntimeError('EfficientNet: Conv2dDynamicSamePadding is not implemented in quantization-friendly way')
    else:
        return partial(Conv2dStaticSamePadding, image_size=image_size)


class Conv2dStaticSamePadding(nn.Module):
    """Efficientnet Conv2dStaticSamePadding re-implementation, with ZeroPad2d replaced with Con2d padding
    (ZeroPad2d is not available for quantization"""

    def __init__(self, in_channels, out_channels, kernel_size, image_size=None, **kwargs):
        super().__init__()
        # super().__init__(in_channels, out_channels, kernel_size, **kwargs)
        self.conv2d = nn.Conv2d(in_channels, out_channels, kernel_size, **kwargs)
        self.conv2d.stride = self.conv2d.stride if len(self.conv2d.stride) == 2 else [self.conv2d.stride[0]] * 2

        try:
            self.conv2d.padding = kernel_size[0] // 2, kernel_size[1] // 2
        except TypeError:
            self.conv2d.padding = kernel_size // 2, kernel_size // 2

    def forward(self, x):
        x = self.conv2d(x)
        return x


class Swish(nn.Module):
    def __init__(self):
        super().__init__()
        self.f_mul = FloatFunctional()

    def forward(self, x):
        return self.f_mul.mul(x, torch.sigmoid(x))


def load_imagenet_pretrained(model, model_name):
    """ Loads pretrained weights, and downloads if loading for the first time. """

    state_dict = model_zoo.load_url(url_map[model_name])
    state_dict.pop('_fc.weight')
    state_dict.pop('_fc.bias')
    new_state_dict = {}
    for k, v in state_dict.items():
        name = k.replace('module.', '')  # remove `module.`
        if name.startswith('feature_extractor._conv_stem.'):
            name = name.replace('feature_extractor._conv_stem.', 'feature_extractor._conv_stem.conv2d.')
        elif name.startswith('feature_extractor._conv_head.'):
            name = name.replace('feature_extractor._conv_head.', 'feature_extractor._conv_head.conv2d.')
        elif name.startswith('feature_extractor._blocks') and '._bn' not in name:
            name = name.replace('.weight', '.conv2d.weight')
            name = name.replace('.bias', '.conv2d.bias')
        new_state_dict[name] = v
    model.load_state_dict(new_state_dict, strict=False)
    print('Loaded pretrained weights for {}'.format(model_name))
