from torch import nn

from efficientnet.utils import (
    round_filters,
    round_repeats,
    get_model_params,
    GlobalParams, BlockArgs,
    MemoryEfficientSwish
)
from qefficientnet.mb_conv_block import MBConvBlock
from qefficientnet.utils import get_same_padding_conv2d, Swish, \
    load_imagenet_pretrained


def create(version, imagenet_pretrained=False,
           in_channels=3, other_params=None, memory_efficient_swish=True):
    QEfficientNet.check_model_name_is_valid(version)

    blocks_args, global_params = get_model_params(version, other_params)
    model = QEfficientNet(blocks_args=blocks_args,
                          global_params=global_params)

    if imagenet_pretrained:
        load_imagenet_pretrained(model, version)

    if in_channels != 3:
        Conv2d = get_same_padding_conv2d(image_size=model._global_params.image_size)
        out_channels = round_filters(32, model._global_params)
        model._conv_stem = Conv2d(in_channels, out_channels, kernel_size=3, stride=2, bias=False)

    if not memory_efficient_swish:
        model.set_swish(memory_efficient=False)

    return model


class QEfficientNet(nn.Module):
    """
    Re-implemented block from EfficientNet repo.
    1) Multiplication and addition operations were replaced with
    quantization-friendly FloatFunctional.
    3) Other MBConvBlocks are used: they contain Conv2D instance instead of conv2d() function call
    4) Paddings changed to be symmetrical about kernel size (3x3 kernel => 1x1 padding; 5x5 kernel => 2x2 padding).
    No more ZeroPad2D
    """

    def __init__(self,
                 blocks_args: BlockArgs = None,
                 global_params: GlobalParams = None):
        super().__init__()
        assert isinstance(blocks_args, list), 'blocks_args should be a list'
        assert len(blocks_args) > 0, 'block args must be greater than 0'
        self._global_params = global_params
        self._blocks_args = blocks_args

        Conv2d = get_same_padding_conv2d(image_size=global_params.image_size)

        self._swish = MemoryEfficientSwish()

        # Batch norm parameters
        bn_mom = 1 - self._global_params.batch_norm_momentum
        bn_eps = self._global_params.batch_norm_epsilon

        # Stem
        in_channels = 3  # rgb
        out_channels = round_filters(32, self._global_params)  # number of output channels
        self._conv_stem = Conv2d(in_channels, out_channels, kernel_size=3, stride=2, bias=False)
        self._bn0 = nn.BatchNorm2d(num_features=out_channels, momentum=bn_mom, eps=bn_eps)

        # Build blocks
        self._blocks = nn.ModuleList([])
        for block_args in self._blocks_args:

            # Update block input and output filters based on depth multiplier.
            block_args = block_args._replace(
                input_filters=round_filters(block_args.input_filters, self._global_params),
                output_filters=round_filters(block_args.output_filters, self._global_params),
                num_repeat=round_repeats(block_args.num_repeat, self._global_params)
            )

            # The first block needs to take care of stride and filter size increase.
            self._blocks.append(MBConvBlock(block_args, self._global_params))
            if block_args.num_repeat > 1:
                block_args = block_args._replace(input_filters=block_args.output_filters, stride=1)
            for _ in range(block_args.num_repeat - 1):
                self._blocks.append(MBConvBlock(block_args, self._global_params))

        # Head
        in_channels = block_args.output_filters  # output of final block
        out_channels = round_filters(1280, self._global_params)
        self._conv_head = Conv2d(in_channels, out_channels, kernel_size=1, bias=False)
        self._bn1 = nn.BatchNorm2d(num_features=out_channels, momentum=bn_mom, eps=bn_eps)

        # Final linear layer
        self._avg_pooling = nn.AdaptiveAvgPool2d(1)
        self._dropout = nn.Dropout(self._global_params.dropout_rate)
        self._fc = nn.Linear(out_channels, self._global_params.num_classes)

    def set_swish(self, memory_efficient=True):
        """Sets swish function as memory efficient (for training) or standard (for export)"""
        self._swish = MemoryEfficientSwish() if memory_efficient else Swish()
        for block in self._blocks:
            block.set_swish(memory_efficient)

    def extract_features(self, inputs):
        """ Returns output of the final convolution layer """

        # Something that is called stem in an original model
        x = self._swish(self._bn0(self._conv_stem(inputs)))

        # Blocks
        for idx, block in enumerate(self._blocks):
            drop_connect_rate = self._global_params.drop_connect_rate
            if drop_connect_rate:
                drop_connect_rate *= float(idx) / len(self._blocks)
            x = block(x, drop_connect_rate=drop_connect_rate)

        # Head
        x = self._swish(self._bn1(self._conv_head(x)))
        return x

    def forward(self, inputs):
        """ Calls extract_features to extract features, applies final linear layer, and returns logits. """
        bs = inputs.size(0)
        # Convolution layers

        x = self.extract_features(inputs)

        # Embedding
        x = self._avg_pooling(x)
        x = x.view(bs, -1)

        x = self._dropout(x)
        x = self._fc(x)

        return x

    @classmethod
    def check_model_name_is_valid(cls, model_name):
        """ Validates model name. """
        valid_models = ['efficientnet-b' + str(i) for i in range(9)]
        if model_name not in valid_models:
            raise ValueError('EfficientNet model version should be one of: ' + ', '.join(valid_models))